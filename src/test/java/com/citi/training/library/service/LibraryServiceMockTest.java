package com.citi.training.library.service;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import com.citi.training.library.dao.LibraryMongoRepo;
import com.citi.training.library.model.Library;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
@SpringBootTest

public class LibraryServiceMockTest {
    private static final Logger LOG = LoggerFactory.getLogger(LibraryServiceMockTest.class);
    
    @Autowired
    private LibraryService libraryService;

    @MockBean
    private LibraryMongoRepo mockLibraryRepo;

    @Test
    public void test_libraryService_save(){
        Library testLib = new Library();
        testLib.setName("Harry Potter");
        testLib.setTypeOfItem(Library.TypeOfItem.DVD);
        testLib.setCurrentlyBorrowed(false);

        // Tell mockRepo that employeeService is going to call save()
        // When it does, return testEmployee object
        when(mockLibraryRepo.save(testLib)).thenReturn(testLib);
        LOG.info("Testing save() with mock repo");
        libraryService.save(testLib);
    }
    @Test
    public void test_libraryService_findAll(){
        // Tell mockRepo that employeeService is going to call findAll()
        // When it does, return the list of type Library
        Library testLib = new Library();
        testLib.setName("Harry Potter");
        testLib.setTypeOfItem(Library.TypeOfItem.DVD);
        testLib.setCurrentlyBorrowed(false);
        List<Library> libList = new ArrayList<Library>();
        libList.add(testLib);
        when(mockLibraryRepo.findAll()).thenReturn(libList);
        LOG.info("Testing findAll() with mock repo");
        assert(libraryService.findAll().size() > 0);
    }

    /* @Test
    public void test_libraryService_remove(){
        Library testLib = new Library();
        testLib.setName("Harry Potter");
        testLib.setTypeOfItem(Library.TypeOfItem.DVD);
        testLib.setCurrentlyBorrowed(false);

        List<Library> libList = new ArrayList<Library>();


       
        when(mockLibraryRepo.delete(testLib)).thenReturn(libList);
        LOG.info("Testing save() with mock repo");
        libraryService.remove(testLib);
    } */
    
}
