package com.citi.training.library.service;

import com.citi.training.library.model.Library;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
@SpringBootTest



public class LibraryServicesTests {
    @Autowired
    private LibraryService libServices;

    @Test
    public void test_save(){
        Library testLibrary = new Library();
        testLibrary.setTypeOfItem(Library.TypeOfItem.DVD);
        testLibrary.setCurrentlyBorrowed(false);
        testLibrary.setName("Harry Potter");

        libServices.save(testLibrary);

    }

    @Test
    public void test_findAll(){
        assert(libServices.findAll().size() > 0);
    
    }


}
