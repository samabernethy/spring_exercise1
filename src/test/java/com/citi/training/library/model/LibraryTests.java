package com.citi.training.library.model;

import org.junit.jupiter.api.Test;

public class LibraryTests {
    private static Library testLib1 = new Library();
    
    //test set name 
    @Test
    public void test_SetName(){
        testLib1.setName("ABC");
        assert(testLib1.getName() == "ABC");
    }
    //test set type
    @Test
    public void test_SetType(){
        testLib1.setTypeOfItem(Library.TypeOfItem.DVD);
        assert(testLib1.getTypeOfItem() == Library.TypeOfItem.DVD);
    }

    //test set Currently Borrowed
    @Test
    public void test_SetCurrentlyBorrowed(){
        testLib1.setCurrentlyBorrowed(false);
        assert(testLib1.isCurrentlyBorrowed() == false);
    }

    //test toString
    

    
}
