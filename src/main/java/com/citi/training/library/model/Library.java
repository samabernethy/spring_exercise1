package com.citi.training.library.model;

import org.springframework.stereotype.Component;

/**
 * Object class for library, each library Item has attributes:
 * TypeOfItem typeOfItem which is an enum of BOOK, CD, DVD or PERIODICAL,
 * boolean currentlyBorrowed,
 * String name.
 * Each library item shall be stored in a repository, using @see LibraryServices
 * & @see LibraryMongoRepo
 * 
 * @author Sam
 * 
 */

@Component
public class Library {
    private TypeOfItem typeOfItem;
    private boolean currentlyBorrowed;
    private String name;

    public TypeOfItem getTypeOfItem() {
        return typeOfItem;
    }

    public void setTypeOfItem(TypeOfItem typeOfItem) {
        this.typeOfItem = typeOfItem;
    }

    public boolean isCurrentlyBorrowed() {
        return currentlyBorrowed;
    }

    public void setCurrentlyBorrowed(boolean currentlyBorrowed) {
        this.currentlyBorrowed = currentlyBorrowed;
    }
    
    public enum TypeOfItem {
        BOOK, CD, DVD, PERIODICALS;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Library [currentlyBorrowed=" + currentlyBorrowed + ", name=" + name + ", typeOfItem=" + typeOfItem
                + "]";
    }
    

    

}
