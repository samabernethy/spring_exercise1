package com.citi.training.library.dao;

import com.citi.training.library.model.Library;


import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

/**
 * Library Mongo Repo that extends from spring Mongo Repo
 * 
 * @author Sam
 */
@Component
public interface LibraryMongoRepo extends MongoRepository<Library, String> {
    
}
