package com.citi.training.library.service;

import java.util.List;

import com.citi.training.library.dao.LibraryMongoRepo;
import com.citi.training.library.model.Library;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This is the main business logic class for Library.
 * 
 * @author Sam
 * @see Library
 */


@Component
public class LibraryService {
    
    @Autowired
    private LibraryMongoRepo libMongoRepo;

    public List<Library> findAll(){
        return libMongoRepo.findAll();
    }

    public Library save(Library lib){
        return libMongoRepo.save(lib);
    }

    public List<Library> remove(Library lib){
      
        libMongoRepo.delete(lib);
        return libMongoRepo.findAll();
    }
    



}
